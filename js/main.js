PlayState = {};

function Hero(game, x, y) {
    //call Phaser.Sprite constructor
    Phaser.Sprite.call(this, game, x, y, 'hero');

    //anchor is the point where Sprites & Images are handled
    this.anchor.set(0.5, 0.5);

    //create a body for the character
    this.game.physics.enable(this);

    //Prevent the main character from moving off the screen
    this.body.collideWorldBounds = true;
}

//Inherit from Phaser.Sprite
Hero.prototype = Object.create(Phaser.Sprite.prototype);
Hero.prototype.constructor = Hero;

//move method for the hero
Hero.prototype.move = function (direction) {
    this.x += direction * 2.5;//2.5 pixels each frame

    //modify the Sprites Velocity so it can move left or right
    const SPEED = 200;
    this.body.velocity.x = direction * SPEED;
}

PlayState.preload = function() {
    //load the background image from the images folder
    this.game.load.image('background', 'images/background.png');
    
    //load the level data in the preload method
    this.game.load.json('level:1', 'data/level01.json');

    //load the images that the platform will use
    this.game.load.image('ground', 'images/ground.png');
    this.game.load.image('grass:8x1', 'images/grass_8x1.png');
    this.game.load.image('grass:6x1', 'images/grass_6x1.png');
    this.game.load.image('grass:4x1', 'images/grass_4x1.png');
    this.game.load.image('grass:2x1', 'images/grass_2x1.png');
    this.game.load.image('grass:1x1', 'images/grass_1x1.png');

    //load the hero image
    this.game.load.image('hero', 'images/hero_stopped.png');
}

PlayState.create = function() {
    this.game.add.image(0, 0, 'background');
    this._loadLevel(this.game.cache.getJSON('level:1'));
}

PlayState._loadLevel = function (data) {

    //spawn all platforms
    data.platforms.forEach(this._spawnPlatform, this);

    //spawn hero and enemies
    this._spawnCharacters({hero: data.hero});
}

PlayState._spawnCharacters = function (data) {
    //spawn hero
    this.hero = new Hero(this.game, data.hero.x, data.hero.y);
    this.game.add.existing(this.hero);
}

PlayState._spawnPlatform = function (platform) {
    this.game.add.sprite(platform.x, platform.y, platform.image);
}

window.onload = function () {
    let game = new Phaser.Game(960, 600, Phaser.AUTO, 'game');
    game.state.add('play', PlayState);
    game.state.start('play');
};

PlayState.init = function() {
    this.game.renderer.renderSession.roundPixels = true;

    this.keys = this.game.input.keyboard.addKeys({
        left: Phaser.KeyCode.LEFT,
        right: Phaser.KeyCode.RIGHT
    });
}

PlayState.update = function() {
    this._handleInput();
}

PlayState._handleInput = function() {
    if(this.keys.left.isDown) {
        this.hero.move(-1);
    }
    else if(this.keys.right.isDown) {
        this.hero.move(1);
    }
    //stop the character from moving when lifting the arrow key
    else {
        this.hero.move(0);
    }
}

function Car() {
    //call parent constructor
    Vehicle.call(this);
}

// clone Vehicle's prototype into Car
Car.prototype = Object.create(Vehicle.prototype);
//restore the constructor at Car
Car.prototype.constructor = Car;